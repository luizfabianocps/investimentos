package br.com.investimento.repositories;

import br.com.investimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Integer> {

}
