package br.com.investimento.controllers;

import br.com.investimento.models.Simulacao;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    public SimulacaoController(){

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Simulacao registraSimulacaoEnviaParaService(@RequestBody @Valid Simulacao simulacao){

        return simulacaoService.salvaSimulacaoNoBanco(simulacao);

    }

    @GetMapping
    public Iterable<Simulacao> leTodasAsSimulacoes(){
        return simulacaoService.lerTodosAsSimulacoes();
    }



    public SimulacaoService getSimulacaoController() {
        return simulacaoService;
    }

    public void setSimulacaoController(SimulacaoController simulacaoController) {
        this.simulacaoService = simulacaoService;
    }


}
