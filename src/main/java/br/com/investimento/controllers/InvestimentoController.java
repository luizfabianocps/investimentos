package br.com.investimento.controllers;



import br.com.investimento.DTOs.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    public InvestimentoController(){

    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento registraInvestimentoEnviaParaService(@RequestBody @Valid Investimento investimento){

        Investimento objetoInvestimento = investimentoService.salvaInvestimentoNoBanco(investimento);
        return objetoInvestimento;
    }

    @GetMapping
    public Iterable<Investimento> lerTodosOsInvestimentos(){
        return investimentoService.lerTodosOsInvestimentos();

    }

    @PostMapping("/simulacao/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTO gerarSimulacao(@RequestBody @PathVariable(name = "id") int id, Simulacao simulacao){

        return investimentoService.gerarSimulacaoEmInvestimentos(id,simulacao);

    }


}
