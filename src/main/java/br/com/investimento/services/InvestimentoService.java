package br.com.investimento.services;


import br.com.investimento.DTOs.SimulacaoDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Optional;

@Service
public class InvestimentoService {


    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoService simulacaoService;


    //Método que salva no banco de dados
    public Investimento salvaInvestimentoNoBanco(Investimento investimento){

        return investimentoRepository.save(investimento);
    }

    //Método que retorna todos os itens da lista.
    public Iterable<Investimento> lerTodosOsInvestimentos(){

        return investimentoRepository.findAll();
    }

    public SimulacaoDTO gerarSimulacaoEmInvestimentos(int id, Simulacao simulacao){


        return simulacaoService.geraSimulacao(simulacao);

    }








    public InvestimentoRepository getInvestimentoRepository() {

        return investimentoRepository;
    }

    public void setInvestimentoRepository(InvestimentoRepository investimentoRepository) {

        this.investimentoRepository = investimentoRepository;
    }

    public SimulacaoService getSimulacaoService() {
        return simulacaoService;
    }

    public void setSimulacaoService(SimulacaoService simulacaoService) {
        this.simulacaoService = simulacaoService;
    }
}
