package br.com.investimento.services;


import br.com.investimento.DTOs.SimulacaoDTO;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.InvestimentoRepository;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {


    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public SimulacaoService() {

    }

    //Método para salvar as informações de simulação no banco
    public Simulacao salvaSimulacaoNoBanco(Simulacao simulacao){
        Simulacao simulacaoDB = simulacaoRepository.save(simulacao);
        return simulacaoDB;
    }

    //Método que retorna todos os itens da lista.
    public Iterable<Simulacao> lerTodosAsSimulacoes(){
        return simulacaoRepository.findAll();
    }


    //Método para calcular o Montante.
    public SimulacaoDTO geraSimulacao(Simulacao simulacao){
        SimulacaoDTO simulacaoDTO = new SimulacaoDTO();
        double rendimento = simulacao.getInvestimento().getRendimentoAoMes();
        double c = simulacao.getValorAplicado();
        int t = simulacao.getQuantidadeDeMeses();
        double montante;
        double rendimentoMes;

        //Fórmula do cálculo = M = valorAplicado(1 + Rendimento)^t
        montante = c*(Math.pow ((1+rendimento),t));
        //Fórmula de rendimento
        rendimentoMes = montante - c;

        simulacaoDTO.setMontante(montante);
        simulacaoDTO.setRendimentoPorMes(rendimentoMes);

        return simulacaoDTO;

    }

    public SimulacaoRepository getSimulacaoRepository() {
        return simulacaoRepository;
    }

    public void setSimulacaoRepository(SimulacaoRepository simulacaoRepository) {
        this.simulacaoRepository = simulacaoRepository;
    }
}
