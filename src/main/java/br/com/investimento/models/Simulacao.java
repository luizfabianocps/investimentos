package br.com.investimento.models;


import javax.persistence.*;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Simulacao {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;
    @NotNull(message = "O campo não pode estar nulo")
    @NotBlank(message = "O campo não pode estar vazio")
    private String nomeInteressado;
    @NotNull(message = "O campo não pode estar nulo")
    @NotBlank(message = "O campo não pode estar vazio")
    private String email;
    @Digits(integer = 6, fraction = 2)
    private double valorAplicado;
    private int quantidadeDeMeses;

    //Faz o vinculo com a tabela Investimento.
    @ManyToOne
    private Investimento investimento;

    public Simulacao(){

    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeDeMeses() {
        return quantidadeDeMeses;
    }

    public void setQuantidadeDeMeses(int quantidadeDeMeses) {
        this.quantidadeDeMeses = quantidadeDeMeses;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
