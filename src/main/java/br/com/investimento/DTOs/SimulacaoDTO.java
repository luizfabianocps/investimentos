package br.com.investimento.DTOs;

import br.com.investimento.models.Simulacao;

public class SimulacaoDTO {

    private double montante;
    private double rendimentoPorMes;

    public SimulacaoDTO(){

    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        this.montante = montante;
    }

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }
}
